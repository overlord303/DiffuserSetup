import wx
import wx.lib.scrolledpanel
import wx.lib.newevent
import matplotlib
import matplotlib.pyplot as plt
from matplotlib.backends.backend_wxagg import FigureCanvasWxAgg as FigureCanvas
from matplotlib.figure import Figure
import matplotlib.patches as patches
from matplotlib.backends.backend_wx import NavigationToolbar2Wx
import math
import itertools
import numpy as np
import zmq
import json
import msgpack
import time
from waveform import Waveform 

PlotEvent, EVT_PLOTEVENT = wx.lib.newevent.NewEvent()
InitEvent, EVT_INIT = wx.lib.newevent.NewEvent()
PlotSelectEvent, EVT_PLOTSELECT = wx.lib.newevent.NewEvent()

class PopupDialog(wx.Dialog):
   def __init__(self, parent, title, text): 
      super(PopupDialog, self).__init__(parent, title = title, size = (400,200)) 
      panel = wx.Panel(self)
      self.text = wx.StaticText(panel, label=text, pos=(10,10))
      self.text.Wrap(400)
      self.btn_ok = wx.Button(panel, wx.ID_OK, label = "OK", size = (50,20), pos = (10,100))
      self.btn_cancel = wx.Button(panel, wx.ID_CANCEL, label = "Cancel", size = (80,20), pos = (60,100))


class PlotPanel(wx.Panel):
    def __init__(self, parent, title="", xlabel="", ylabel0="", ylabel1="", xlimits=[0,1], ylimits0=[0,1], ylimits1=[0,1]):
        wx.Panel.__init__(self, parent)

        self.title = title
        self.plots_enabled = [True, False]
        self.waveform0 = [0]*10000
        self.waveform1 = [0]*10000

        self.xlabel = xlabel
        self.ylabels = [ylabel0, ylabel1]
        self.xlimits = xlimits
        self.ylimits = [ylimits0, ylimits1]

        figure = Figure()
        figure.subplots_adjust(left=0.16, bottom=0.14, right=0.84)
        figure.set_size_inches(8/1.75,6/1.75)

        self.axes = figure.add_subplot(1, 1, 1)
        self.axes2 = self.axes.twinx()
        self.updateAxes()

        self.canvas = FigureCanvas(self, -1, figure)

        navToolbar = NavigationToolbar2Wx(self.canvas)
        navToolbar.DeleteToolByPos(8);navToolbar.DeleteToolByPos(6);navToolbar.DeleteToolByPos(2);navToolbar.DeleteToolByPos(1)
        toggleToolbar = wx.ToolBar(self)
        self.toggle_chan0 = toggleToolbar.AddCheckTool(111, '', wx.Bitmap('GUI/0.png'))
        self.toggle_chan1 = toggleToolbar.AddCheckTool(222, '', wx.Bitmap('GUI/1.png'))
        toggleToolbar.ToggleTool(111, True)
        toggleToolbar.Realize()
        self.Bind(wx.EVT_TOOL, lambda event: self.setPlotsEnabled(0, event), self.toggle_chan0)
        self.Bind(wx.EVT_TOOL, lambda event: self.setPlotsEnabled(1, event), self.toggle_chan1)
        
        sizer = wx.BoxSizer(wx.VERTICAL)
        sizer.Add(self.canvas, 1, wx.LEFT | wx.TOP | wx.GROW)
        sizer_horizontal = wx.BoxSizer(wx.HORIZONTAL)
        sizer_horizontal.Add(navToolbar)
        sizer_horizontal.Add(toggleToolbar)
        sizer.Add(sizer_horizontal)
        self.SetSizer(sizer)
        self.Fit()

        self.draw(self.waveform0, self.waveform1)

    def setLimitsX(self, limits):
        self.xlimits = limits 
        self.canvas.draw()
        self.Update()

    def setLimitsY(self, n, limits):
        self.ylimits[n] = limits
        self.canvas.draw()
        self.Update()

    def drawEvent(self, event):
        if event.waveform1 is None:
           return
        self.draw(event.waveform1, event.waveform2)

    def setPlotsEnabled(self, index, event):
        self.plots_enabled[index] = event.GetSelection()
        self.draw(self.waveform0, self.waveform1)

    def updateAxes(self):
        self.axes.clear()
        self.axes2.clear()
        self.axes.yaxis.label.set_color('blue')
        self.axes2.yaxis.label.set_color('red')
        self.axes.set_title(self.title)
        self.axes.set_xlabel(self.xlabel)
        self.axes.set_ylabel(self.ylabels[0])
        self.axes.set_xlim(self.xlimits)
        self.axes.set_ylim(self.ylimits[0])
        self.axes.tick_params(right=False, axis='y', colors='blue')
        self.axes2.set_ylabel(self.ylabels[1])
        self.axes2.set_ylim(self.ylimits[1])
        self.axes2.tick_params(left=False, labelleft=False, labelright=True, axis='y', colors='red')

    def draw(self, waveform0, waveform1):
        self.waveform0 = waveform0
        self.waveform1 = waveform1
        self.updateAxes()

        lines = []
        if self.plots_enabled[0] and self.waveform0 is not None:
            ln0 = self.axes.plot(waveform0, 'blue', label="Input 0")
            lines += ln0
        if self.plots_enabled[1] and self.waveform1 is not None:
            ln1 = self.axes2.plot(waveform1, 'red', label="Input 1")
            lines += ln1
        labels = [l.get_label() for l in lines]

        self.axes.legend(lines, labels, loc='best')
        self.canvas.draw()
        self.Update()


class HeatmapPanel(wx.Panel):
    def __init__(self, parent, title="", xlabel="", ylabel="", zlabel=""):
        wx.Panel.__init__(self, parent)

        self.parent = parent
        self.zlabel = zlabel

        self.figure = Figure()
        self.figure.subplots_adjust(left=0.16, bottom=0.14)
        self.figure.set_size_inches(8/1.75,6/1.75)

        self.axes = self.figure.add_subplot(1,1,1)
        self.axes.set_title(title)
        self.axes.set_xlabel(xlabel)
        self.axes.set_ylabel(ylabel)

        self.patches = []

        self.canvas = FigureCanvas(self, -1, self.figure)
        self.sizer = wx.BoxSizer(wx.VERTICAL)
        self.sizer.Add(self.canvas, 1, wx.LEFT | wx.TOP | wx.GROW)
        self.SetSizer(self.sizer)
        #self.Fit()

        self.resizeGrid([0,1], [0,1], 1, 1)

        self.cid = self.canvas.mpl_connect('button_press_event', self)

    def resizeGrid(self, rangeX, rangeY, stepSizeX, stepSizeY):
        self.stepSizeX = stepSizeX
        self.stepSizeY = stepSizeY

        nGridX = math.ceil(abs(rangeX[1]-rangeX[0])/stepSizeX)+1
        nGridY = math.ceil(abs(rangeY[1]-rangeY[0])/stepSizeY)+1
        self.areaArray = np.zeros((nGridY, nGridX))
        self.waveformArray1 = np.ndarray(shape=(nGridX,nGridY),dtype=object)
        self.waveformArray2 = np.ndarray(shape=(nGridX,nGridY),dtype=object)

        self.xcoords = np.array([rangeX[0] + i*stepSizeX for i in range(nGridX)])
        self.ycoords = np.array([rangeY[0] + i*stepSizeY for i in range(nGridY)])

        plotranges = [self.xcoords[0]-stepSizeX/2., self.xcoords[-1]+stepSizeX/2.,\
                      self.ycoords[0]-stepSizeY/2., self.ycoords[-1]+stepSizeY/2.]       

        self.image = self.axes.imshow(self.areaArray, cmap="RdBu_r",\
                                      extent=plotranges, origin="lower", aspect="auto")

        if not hasattr(self, 'cbar'):
            self.cbar = self.axes.figure.colorbar(self.image, ax=self.axes)
            self.cbar.ax.set_ylabel(self.zlabel, rotation=-90, va="bottom")

        self.canvas.draw()
        self.Update()

    def draw(self):
        self.remove_patches()
        self.image.set_data(self.areaArray)
        self.image.set_clim(np.amin(self.areaArray[self.areaArray != 0]), np.amax(self.areaArray[self.areaArray != 0]))
        self.cbar.remove()
        self.cbar = self.axes.figure.colorbar(self.image, ax=self.axes)
        self.cbar.ax.set_ylabel(self.zlabel, rotation=-90, va="bottom")
        self.canvas.draw()
        self.Update()

    def addArea(self, xval, yval, waveform1, waveform2):
        x_i = (np.abs(self.xcoords - xval)).argmin()
        y_i = (np.abs(self.ycoords - yval)).argmin()
        self.areaArray[y_i, x_i] = waveform1.area
        self.waveformArray1[x_i, y_i] = waveform1.Y
        self.waveformArray2[x_i, y_i] = waveform2.Y
        self.draw()

    def __call__(self,event):
        if event.inaxes != self.axes:
            return
        x_i = (np.abs(self.xcoords - event.xdata)).argmin()
        y_i = (np.abs(self.ycoords - event.ydata)).argmin()

        self.remove_patches()
        rect = patches.Rectangle((self.xcoords[x_i]-self.stepSizeX/2, self.ycoords[y_i]-self.stepSizeY/2),\
                                 self.stepSizeX, self.stepSizeY, linewidth=2, edgecolor='g', facecolor='none')
        self.axes.add_patch(rect)
        self.patches.append(rect)
        self.canvas.draw()

        plotevent = PlotEvent(waveform1=self.waveformArray1[x_i, y_i], waveform2=self.waveformArray2[x_i, y_i])
        wx.PostEvent(self.parent, plotevent)

    def remove_patches(self):
    	for patch in self.patches:
    	    patch.remove()
    	self.patches = []



class RangePanel(wx.Panel):
    def __init__(self, parent, title="", height=100, rows=1):
        wx.Panel.__init__(self, parent)

        titletxt = wx.StaticText(self, label=title)
        titletxt.SetFont(wx.Font(-1, wx.DEFAULT, wx.NORMAL, wx.BOLD, 0, ""))

        self.scrollWindow = wx.ScrolledWindow(self, size=(-1,height-25))
        self.vbox_scroll = wx.BoxSizer(wx.VERTICAL)
        self.scrollWindow.SetSizer(self.vbox_scroll)
        self.scrollWindow.SetScrollbars(0, 1, 0, 1)

        self.n_rows = 0
        for _ in range(rows):
            self.addRow(["0","0"])

        self.scrollWindow.SetSize(self.scrollWindow.GetSize()[0]+10,-1)

        button_add = wx.Button(self, wx.ID_ANY, "Add", size=(50,25))
        button_delete = wx.Button(self, wx.ID_ANY, "Delete", size=(50,25))
        self.Bind(wx.EVT_BUTTON, lambda event: self.addRow(["0","0"], event), button_add)
        self.Bind(wx.EVT_BUTTON, lambda event: self.deleteRow(False, event), button_delete)

        txt_stepsize = wx.StaticText(self, label="Step size ")
        self.field_stepsize = wx.TextCtrl(self, value="1", size=(50,20), style=wx.TE_RIGHT)

        self.button_home = wx.Button(self, wx.ID_ANY, "Home", size=(85,20))
        self.button_zero = wx.Button(self, wx.ID_ANY, "Zero", size=(85,20))

        self.button_moveto = wx.Button(self, wx.ID_ANY, "Move to", size=(85,20))
        self.field_moveto = wx.TextCtrl(self, value="0", size=(50,20), style=wx.TE_RIGHT)

        self.text_currpos = wx.StaticText(self, label="Current position ")
        self.field_currpos = wx.TextCtrl(self, value="", size=(50,20), style=wx.TE_RIGHT)
        self.field_currpos.SetEditable(False)
        self.field_currpos.SetBackgroundColour((200,200,200))

        vbox = wx.BoxSizer(wx.VERTICAL)
        vbox.Add(titletxt, flag=wx.ALIGN_RIGHT)
        vbox.Add(self.scrollWindow, flag=wx.ALIGN_RIGHT)
        hbox = wx.BoxSizer(wx.HORIZONTAL)
        hbox.Add(button_add)
        hbox.Add(button_delete)
        vbox.Add(hbox, flag=wx.ALIGN_RIGHT)
        vbox.Add(wx.StaticLine(self, style=wx.LI_HORIZONTAL), 0, wx.EXPAND|wx.ALL, 20)
        stepbox = wx.BoxSizer(wx.HORIZONTAL)
        stepbox.Add(txt_stepsize)
        stepbox.Add(self.field_stepsize)
        vbox.Add(stepbox, flag=wx.ALIGN_RIGHT)
        vbox.Add(wx.StaticLine(self, style=wx.LI_HORIZONTAL), 0, wx.EXPAND|wx.ALL, 20)
        movebox = wx.BoxSizer(wx.HORIZONTAL)
        movebox.Add(self.button_moveto)
        movebox.AddSpacer(10)
        movebox.Add(self.field_moveto)
        vbox.Add(movebox, flag=wx.ALIGN_RIGHT)
        posbox = wx.BoxSizer(wx.HORIZONTAL)
        posbox.Add(self.text_currpos)
        posbox.Add(self.field_currpos)
        vbox.Add(posbox, flag=wx.ALIGN_RIGHT)
        homezerobox = wx.BoxSizer(wx.HORIZONTAL)
        homezerobox.Add(self.button_home)
        homezerobox.Add(self.button_zero)
        vbox.Add(homezerobox, flag=wx.ALIGN_RIGHT)

        self.SetSizer(vbox)

    def addRow(self, range, event=0):
        self.n_rows += 1
        text1 = wx.StaticText(self.scrollWindow, -1, str(self.n_rows)+": ", size=(25, -1), style=wx.ALIGN_RIGHT)
        range_start = wx.TextCtrl(self.scrollWindow, value=range[0], size=(50,20), style=wx.TE_RIGHT)
        text2 = wx.StaticText(self.scrollWindow, -1, " to ")
        range_end = wx.TextCtrl(self.scrollWindow, value=range[1], size=(50,20), style=wx.TE_RIGHT)
        hbox = wx.BoxSizer(wx.HORIZONTAL)
        hbox.AddMany([text1, range_start, text2, range_end])
        self.vbox_scroll.Add(hbox)
        self.vbox_scroll.Layout()
        w, h = self.vbox_scroll.GetMinSize()
        self.scrollWindow.SetVirtualSize((w, h))

    def deleteRow(self, force=False, event=0):
        minrows = 0 if force else 1
        if self.n_rows > minrows:
            self.vbox_scroll.Hide(self.n_rows-1)
            self.vbox_scroll.Remove(self.n_rows-1)
            self.n_rows -= 1
            w, h = self.vbox_scroll.GetMinSize()
            self.scrollWindow.SetVirtualSize((w, h))

    def getRanges(self, event=0):
        ranges = []
        for hbox in self.vbox_scroll.GetChildren():
            txt_start = hbox.GetSizer().GetItem(1).GetWindow()
            txt_end = hbox.GetSizer().GetItem(3).GetWindow()
            ranges.append( [float(txt_start.GetValue()), float(txt_end.GetValue())] )
        return ranges

    def getRangeExtremes(self, event=0):
        allvalues = list(itertools.chain.from_iterable(self.getRanges()))
        extremes = [min(allvalues), max(allvalues)]
        return extremes

    def getStepSize(self, event=0):
        return float(self.field_stepsize.GetValue())

    def setRangesFromConfig(self, config):
        while self.n_rows > 0:
            self.deleteRow(force=True)
        for rangestr in config.split("|"):
            curr_range = rangestr.split(",")
            if len(curr_range) == 1:
                curr_range = [curr_range[0]]*2
            self.addRow(curr_range)

    def setStepSize(self, value):
        self.field_stepsize.SetValue(value)


class GUI(wx.Frame):
    def __init__(self, *args, **kwargs):
        super(GUI, self).__init__(*args, **kwargs)

        # ZMQ sockets
        self.context = zmq.Context(1)

        self.socket_pull = self.context.socket(zmq.PULL)
        self.socket_pull.bind("tcp://127.0.0.1:5555")
        self.zmqpoller = zmq.Poller()
        self.zmqpoller.register(self.socket_pull, zmq.POLLIN)

        self.socket_pub = self.context.socket(zmq.PUB)
        self.socket_pub.bind("tcp://127.0.0.1:5556")

        self.initUI()

    def initUI(self):

        self.UITimer = wx.Timer(self)
        self.Bind(wx.EVT_TIMER, self._updateUI, self.UITimer)

        self.MonitorTimer = wx.Timer(self)
        self.Bind(wx.EVT_TIMER, self._recordSinglePulse, self.MonitorTimer)

        # Menu bar
        ####################################################

        menu_file = wx.Menu()

        file_openconfig = menu_file.Append(wx.ID_ANY, 'Open', 'Open configuration file')
        self.Bind(wx.EVT_MENU, self.openConfigFile, file_openconfig)

        file_saveconfig = menu_file.Append(wx.ID_ANY, 'Save', 'Save configuration file')
        self.Bind(wx.EVT_MENU, self.saveConfigFile, file_saveconfig)        

        file_close = menu_file.Append(wx.ID_EXIT, 'Quit', 'Quit application')
        self.Bind(wx.EVT_MENU, self.onQuit, file_close)

        menubar = wx.MenuBar()
        menubar.Append(menu_file, '&File')
        self.SetMenuBar(menubar)

        # Plots and buttons
        ####################################################

        self.plotpanel = PlotPanel(self, title="Waveforms", xlabel="Time [ns]", ylabel0="Input 0 [V]", ylabel1="Input 1 [V]", xlimits=[0,10000], ylimits0=[-1.0,1.0], ylimits1=[-1.0,1.0])
        self.heatmappanel = HeatmapPanel(self, title="Waveform areas", xlabel="Angle [°]", ylabel="Height [mm]", zlabel="Signal area [V*ns]")

        self.Bind(EVT_PLOTEVENT, self.plotpanel.drawEvent)

        self.buttonMain = wx.Button(self, wx.ID_ANY, "Initialise\ndevices", size=(100,90))
        self.buttonMain.Bind(wx.EVT_BUTTON, self._initialiseProgram)

        self.buttonMonitor = wx.ToggleButton(self, wx.ID_ANY, "Continuous\nmonitoring", size=(100,90))
        self.buttonMonitor.SetBackgroundColour((200,200,200))

        self.buttonRecordSingle = wx.Button(self, wx.ID_ANY, "Record\npulse", size=(100,90))
        self.buttonRecordSingle.SetBackgroundColour((200,200,200))

        self.angleRangePanel = RangePanel(self, title="Angle ranges (°)", height=150)
        self.yRangePanel = RangePanel(self, title="Height ranges (mm)", height=150)

        txt_size = (125,20)

        self.dict_output = {
            "rootfilename"       : [wx.StaticText(self, label="File name "), wx.TextCtrl(self, size=txt_size)],
            "treename"           : [wx.StaticText(self, label="TTree name "), wx.TextCtrl(self, size=txt_size)],
            "ID_diffuser"        : [wx.StaticText(self, label="Diffuser ID "), wx.TextCtrl(self, size=txt_size)],
            "ID_PMT"             : [wx.StaticText(self, label="PMT ID "), wx.TextCtrl(self, size=txt_size)],
            "ID_PD"              : [wx.StaticText(self, label="Photodiode ID "), wx.TextCtrl(self, size=txt_size)],
            "ID_lightsource"     : [wx.StaticText(self, label="Light source ID "), wx.TextCtrl(self, size=txt_size)],
            "ID_experimentalist" : [wx.StaticText(self, label="Operator name "), wx.TextCtrl(self, size=txt_size)],
            "notes"              : [wx.StaticText(self, label="Notes "), wx.TextCtrl(self, size=(txt_size[0],100), style=wx.TE_MULTILINE)],
        }

        self.dict_funcgen = {
            "funcgen_channel"   : [wx.StaticText(self, label="Output channel "), wx.ComboBox(self, size=txt_size, choices=["1","2"])],
            "funcgen_cycles"    : [wx.StaticText(self, label="Number of cycles "), wx.TextCtrl(self, size=txt_size)],
            "funcgen_frequency" : [wx.StaticText(self, label="Frequency [Hz] "), wx.TextCtrl(self, size=txt_size)],
            "funcgen_Vmin"      : [wx.StaticText(self, label="Min output [V] "), wx.TextCtrl(self, size=txt_size)],
            "funcgen_Vmax"      : [wx.StaticText(self, label="Max output [V] "), wx.TextCtrl(self, size=txt_size)],
        }

        self.dict_devices = {
            "linmotor_devicename" : [wx.StaticText(self, label="Linear motor device name "), wx.TextCtrl(self, size=txt_size)],
            "rotmotor_USBport"    : [wx.StaticText(self, label="Angular motor USB name "), wx.TextCtrl(self, size=txt_size)],
            "humid_USBport"       : [wx.StaticText(self, label="Humidity sensor USB name "), wx.TextCtrl(self, size=txt_size)],
            "funcgen_IP"          : [wx.StaticText(self, label="Function generator IP "), wx.TextCtrl(self, size=txt_size)],
        }

        self.dict_digitizer = {
            "digitizer_numChannels"         : [wx.StaticText(self, label="Channels active "), wx.ComboBox(self, size=txt_size, value="2", choices=["1","2"])],
            "digitizer_numSamples"          : [wx.StaticText(self, label="Number of samples "), wx.TextCtrl(self, size=txt_size)],
            "digitizer_sampleRate"          : [wx.StaticText(self, label="Sample rate [MHz] "), wx.ComboBox(self, size=txt_size, choices=["2500","1000","500","250","100","50","25","10","5","1"])],
            "digitizer_triggerLevel"        : [wx.StaticText(self, label="Trigger level [mV] "), wx.TextCtrl(self, size=txt_size)],
            "digitizer_inputRange0"         : [wx.StaticText(self, label="Input range 0 [mV] "), wx.ComboBox(self, size=txt_size, choices=["200","500","1000","2500"])],
            "digitizer_inputRange1"         : [wx.StaticText(self, label="Input range 1 [mV] "), wx.ComboBox(self, size=txt_size, choices=["200","500","1000","2500"])],
            "digitizer_inputOffsetPercent0" : [wx.StaticText(self, label="Input offset 0 [%] "), wx.TextCtrl(self, size=txt_size)],
            "digitizer_inputOffsetPercent1" : [wx.StaticText(self, label="Input offset 1 [%] "), wx.TextCtrl(self, size=txt_size)],
        }

        self.dict_digitizer["digitizer_numChannels"][1].Bind(wx.EVT_COMBOBOX, self._toggleChannels)

        self.dict_other = {
            "linmotor_steps_unit" : "1000",
            "rotmotor_steps_unit" : "1000",
            "rotmotor_n_motor"    : "0",
            "funcgen_shape"       : "SQUARE",
        }

        # Visual layout
        ####################################################

        vbox = wx.BoxSizer(wx.VERTICAL) # Main vertical sizer
        
        plotbox = wx.BoxSizer(wx.HORIZONTAL) # Horizontal sizer for plots
        plotbox.Add(self.plotpanel, flag=wx.LEFT)
        plotbox.Add(wx.StaticLine(self, style=wx.LI_VERTICAL), 0, wx.EXPAND|wx.ALL, 10)
        plotbox.Add(self.heatmappanel, flag=wx.RIGHT)

        outputbox = wx.BoxSizer(wx.VERTICAL)
        title = wx.StaticText(self, label="Output")
        title.SetFont(wx.Font(-1, wx.DEFAULT, wx.NORMAL, wx.BOLD, 0, ""))
        outputbox.Add(title, flag=wx.ALIGN_RIGHT)
        for line in self.dict_output.values():
            sizer = wx.BoxSizer(wx.HORIZONTAL)
            sizer.AddMany(line)
            outputbox.Add(sizer, flag=wx.ALIGN_RIGHT)

        funcgenbox = wx.BoxSizer(wx.VERTICAL)
        title = wx.StaticText(self, label="Function generator")
        title.SetFont(wx.Font(-1, wx.DEFAULT, wx.NORMAL, wx.BOLD, 0, ""))
        funcgenbox.Add(title, flag=wx.ALIGN_RIGHT)
        for line in self.dict_funcgen.values():
            sizer = wx.BoxSizer(wx.HORIZONTAL)
            sizer.AddMany(line)
            funcgenbox.Add(sizer, flag=wx.ALIGN_RIGHT)

        devicebox = wx.BoxSizer(wx.VERTICAL)
        title = wx.StaticText(self, label="Device initialisation")
        title.SetFont(wx.Font(-1, wx.DEFAULT, wx.NORMAL, wx.BOLD, 0, ""))
        devicebox.Add(title, flag=wx.ALIGN_RIGHT)
        for line in self.dict_devices.values():
            sizer = wx.BoxSizer(wx.HORIZONTAL)
            sizer.AddMany(line)
            devicebox.Add(sizer, flag=wx.ALIGN_RIGHT)

        digitizerbox = wx.BoxSizer(wx.VERTICAL)
        title = wx.StaticText(self, label="Digitizer settings")
        title.SetFont(wx.Font(-1, wx.DEFAULT, wx.NORMAL, wx.BOLD, 0, ""))
        digitizerbox.Add(title, flag=wx.ALIGN_RIGHT)
        for line in self.dict_digitizer.values():
            sizer = wx.BoxSizer(wx.HORIZONTAL)
            sizer.AddMany(line)
            digitizerbox.Add(sizer, flag=wx.ALIGN_RIGHT)

        func_device_box = wx.BoxSizer(wx.VERTICAL)
        func_device_box.Add(devicebox, flag=wx.ALIGN_RIGHT)
        func_device_box.Add(wx.StaticLine(self, style=wx.LI_HORIZONTAL), 0, wx.EXPAND|wx.ALL, 10)
        func_device_box.Add(funcgenbox, flag=wx.ALIGN_RIGHT)
        func_device_box.Add(wx.StaticLine(self, style=wx.LI_HORIZONTAL), 0, wx.EXPAND|wx.ALL, 10)
        func_device_box.Add(digitizerbox, flag=wx.ALIGN_RIGHT)        
        func_device_box.AddSpacer(10)

        rangebox = wx.BoxSizer(wx.HORIZONTAL)
        rangebox.Add(self.angleRangePanel)
        rangebox.Add(wx.StaticLine(self, style=wx.LI_VERTICAL), 0, wx.EXPAND|wx.ALL, 10)
        rangebox.Add(self.yRangePanel)

        buttonbox = wx.BoxSizer(wx.HORIZONTAL)
        buttonbox.Add(self.buttonMain)
        buttonbox.Add(self.buttonMonitor)
        buttonbox.Add(self.buttonRecordSingle)

        ctrlbox = wx.BoxSizer(wx.VERTICAL)
        ctrlbox.Add(rangebox)
        ctrlbox.Add(wx.StaticLine(self, style=wx.LI_HORIZONTAL), 0, wx.EXPAND|wx.ALL, 10)
        ctrlbox.Add(buttonbox, flag=wx.BOTTOM)

        configbox = wx.BoxSizer(wx.HORIZONTAL)
        configbox.AddSpacer(10)
        configbox.Add(ctrlbox)
        configbox.Add(wx.StaticLine(self, style=wx.LI_VERTICAL), 0, wx.EXPAND|wx.ALL, 10)
        configbox.Add(func_device_box)
        configbox.Add(wx.StaticLine(self, style=wx.LI_VERTICAL), 0, wx.EXPAND|wx.ALL, 10)
        configbox.Add(outputbox)
        configbox.AddSpacer(10)

        vbox.Add(plotbox, flag=wx.TOP)
        vbox.AddSpacer(10)
        vbox.Add(configbox)

        self.SetSizerAndFit(vbox)

    def _toggleChannels(self, event=0):
        n_chan = int(self.dict_digitizer["digitizer_numChannels"][1].GetValue())
        samplerate = self.dict_digitizer["digitizer_sampleRate"][1].GetValue().split('.')[0]
        if n_chan == 1:
            self.dict_digitizer["digitizer_inputRange1"][1].SetEditable(False)
            self.dict_digitizer["digitizer_inputRange1"][1].SetBackgroundColour((200,200,200))
            self.dict_digitizer["digitizer_inputOffsetPercent1"][1].SetEditable(False)
            self.dict_digitizer["digitizer_inputOffsetPercent1"][1].SetBackgroundColour((200,200,200))
            self.dict_digitizer["digitizer_sampleRate"][1].Clear()
            self.dict_digitizer["digitizer_sampleRate"][1].AppendItems(["5000","2500","1000","500","250","100","50","25","10","5","1"])
        else:
            self.dict_digitizer["digitizer_inputRange1"][1].SetEditable(True)
            self.dict_digitizer["digitizer_inputRange1"][1].SetBackgroundColour((255,255,255))
            self.dict_digitizer["digitizer_inputOffsetPercent1"][1].SetEditable(True)
            self.dict_digitizer["digitizer_inputOffsetPercent1"][1].SetBackgroundColour((255,255,255))
            self.dict_digitizer["digitizer_sampleRate"][1].Clear()
            self.dict_digitizer["digitizer_sampleRate"][1].AppendItems(["2500","1000","500","250","100","50","25","10","5","1"])

        self.dict_digitizer["digitizer_sampleRate"][1].SetValue(samplerate)

    def _updateUI(self, event):
        socks = dict(self.zmqpoller.poll(0))
        if self.socket_pull in socks and socks[self.socket_pull] == zmq.POLLIN:
            message = self.socket_pull.recv()
            msgtuple = msgpack.unpackb(message, use_list=False)
            if len(msgtuple) == 2:
                angle, ypos = msgtuple
            else:
                mode, angle, ypos, waveform_PMT, waveform_PD = msgtuple
                waveform_PMT = np.array(waveform_PMT)
                waveform_PD = np.array(waveform_PD)
                self.plotpanel.draw(waveform_PMT, waveform_PD)
                if mode == "multi":
                    timevec = np.arange(len(waveform_PMT))/float(self.dict_digitizer["digitizer_sampleRate"][1].GetValue())*1.e3
                    wf1 = Waveform(timevec, waveform_PMT)
                    wf2 = Waveform(timevec, waveform_PD)
                    self.heatmappanel.addArea(angle, ypos, wf1, wf2)

            self.angleRangePanel.field_currpos.SetValue(str(angle))
            self.yRangePanel.field_currpos.SetValue(str(ypos))

    def _constructConfigDict(self):
        config_dict = {}
        angleRangesStr = [[str(x[0]), str(x[1])] for x in self.angleRangePanel.getRanges()]
        yRangesStr = [[str(x[0]), str(x[1])] for x in self.yRangePanel.getRanges()]
        config_dict["rangesAngle"] = "|".join([",".join(x) for x in angleRangesStr])
        config_dict["rangesY"] = "|".join([",".join(x) for x in yRangesStr])
        config_dict["stepSizeAngle"] = str(self.angleRangePanel.getStepSize())
        config_dict["stepSizeY"] = str(self.yRangePanel.getStepSize())

        for key, value in {**self.dict_output, **self.dict_funcgen, **self.dict_digitizer, **self.dict_devices}.items():
            config_dict[key] = value[1].GetValue()

        for key, value in self.dict_other.items():
            config_dict[key] = value

        config_dict["digitizer_sampleRate"] = str(int(config_dict["digitizer_sampleRate"])*int(1.e6))

        return config_dict

    def _constructConfigDict_initialise(self):
        config_dict = {}
        for key, value in self.dict_devices.items():
            config_dict[key] = value[1].GetValue()
        for key, value in self.dict_other.items():
            config_dict[key] = value
        return config_dict

    def _constructConfigDict_linmotor(self, cmd):
        config_dict = {}
        if cmd == "move":
            config_dict["move_linmotor"] = self.yRangePanel.field_moveto.GetValue()
        elif cmd == "home":
            config_dict["home_linmotor"] = "1"
        elif cmd == "zero":
            config_dict["zero_linmotor"] = "1"

        return config_dict

    def _constructConfigDict_rotmotor(self, cmd):
        config_dict = {}
        if cmd == "move":
            config_dict["move_rotmotor"] = self.angleRangePanel.field_moveto.GetValue()
        elif cmd == "home":
            config_dict["home_rotmotor"] = "1"
        elif cmd == "zero":
            config_dict["zero_rotmotor"] = "1"
        
        return config_dict

    def _constructConfigDict_recordsingle(self):
        config_dict = {}
        config_dict["record_single"] = "1"
        for key, value in {**self.dict_funcgen, **self.dict_digitizer}.items():
            config_dict[key] = value[1].GetValue()

        for key, value in self.dict_other.items():
            config_dict[key] = value

        config_dict["digitizer_sampleRate"] = str(int(config_dict["digitizer_sampleRate"])*int(1.e6))

        return config_dict

    def _initialiseProgram(self, event):
        config_dict = self._constructConfigDict_initialise()
        self.socket_pub.send_string(json.dumps(config_dict))

        # Make device fields grey and uneditable
        for value in self.dict_devices.values(): 
            value[1].SetEditable(False)
            value[1].SetBackgroundColour((200,200,200))

        self.buttonMain.SetLabel("Start\nscan")
        self.buttonMain.Unbind(wx.EVT_BUTTON)
        self.buttonMain.Bind(wx.EVT_BUTTON, self._startstopProgram)
        self.buttonMonitor.Bind(wx.EVT_TOGGLEBUTTON, self._continuousMonitoring)
        self.buttonMonitor.SetBackgroundColour((247, 247, 247, 255))
        self.buttonRecordSingle.Bind(wx.EVT_BUTTON, self._recordSinglePulse)
        self.buttonRecordSingle.SetBackgroundColour((247, 247, 247, 255))
        self.Bind(wx.EVT_BUTTON, lambda event: self._moveLinMotor("move", event), self.yRangePanel.button_moveto)
        self.Bind(wx.EVT_BUTTON, lambda event: self._moveLinMotor("home", event), self.yRangePanel.button_home)
        self.Bind(wx.EVT_BUTTON, lambda event: self._moveLinMotor("zero", event), self.yRangePanel.button_zero)
        self.Bind(wx.EVT_BUTTON, lambda event: self._moveRotMotor("move", event), self.angleRangePanel.button_moveto)
        self.Bind(wx.EVT_BUTTON, lambda event: self._moveRotMotor("home", event), self.angleRangePanel.button_home)
        self.Bind(wx.EVT_BUTTON, lambda event: self._moveRotMotor("zero", event), self.angleRangePanel.button_zero)
        self.UITimer.Start(100)

    def _startstopProgram(self, event):
        self.MonitorTimer.Stop()
        self.buttonMonitor.SetValue(False)
        time.sleep(0.1)
        config_dict = self._constructConfigDict()
        self.socket_pub.send_string(json.dumps(config_dict))

        self.heatmappanel.resizeGrid(self.angleRangePanel.getRangeExtremes(),
                                     self.yRangePanel.getRangeExtremes(),
                                     self.angleRangePanel.getStepSize(),
                                     self.yRangePanel.getStepSize())

        inputMax0 = int(self.dict_digitizer["digitizer_inputRange0"][1].GetValue())
        inputMax1 = int(self.dict_digitizer["digitizer_inputRange1"][1].GetValue())
        inputOffset0 = 0.01*int(self.dict_digitizer["digitizer_inputOffsetPercent0"][1].GetValue())
        inputOffset1 = 0.01*int(self.dict_digitizer["digitizer_inputOffsetPercent1"][1].GetValue())
        numSamples = int(self.dict_digitizer["digitizer_numSamples"][1].GetValue())
        dt = 1.e3/float(self.dict_digitizer["digitizer_sampleRate"][1].GetValue())
        self.plotpanel.setLimitsX([0,numSamples*dt])
        self.plotpanel.setLimitsY(0, [(-1+inputOffset0)*inputMax0*0.001, (1+inputOffset0)*inputMax0*0.001])
        self.plotpanel.setLimitsY(1, [(-1+inputOffset1)*inputMax1*0.001, (1+inputOffset1)*inputMax1*0.001])

    def _moveLinMotor(self, cmd, event=0):
        if cmd == "home":
            with PopupDialog(self, "Check cables", "Make sure cables have sufficient slack during homing!") as dlg:
                if dlg.ShowModal() != wx.ID_OK:
                    return
        config_dict = self._constructConfigDict_linmotor(cmd)
        self.socket_pub.send_string(json.dumps(config_dict))

    def _moveRotMotor(self, cmd, event=0):
        if cmd == "home":
            with PopupDialog(self, "Check cables", "Make sure cables have sufficient slack during homing!") as dlg:
                if dlg.ShowModal() != wx.ID_OK:
                    return
        config_dict = self._constructConfigDict_rotmotor(cmd)
        self.socket_pub.send_string(json.dumps(config_dict))

    def _continuousMonitoring(self, event):
        if event.GetSelection():
            self.MonitorTimer.Start(1000)
        else:
            self.MonitorTimer.Stop()

    def _recordSinglePulse(self, event=0):
        config_dict = self._constructConfigDict_recordsingle()
        self.socket_pub.send_string(json.dumps(config_dict))
        inputMax0 = int(self.dict_digitizer["digitizer_inputRange0"][1].GetValue())
        inputMax1 = int(self.dict_digitizer["digitizer_inputRange1"][1].GetValue())
        inputOffset0 = 0.01*int(self.dict_digitizer["digitizer_inputOffsetPercent0"][1].GetValue())
        inputOffset1 = 0.01*int(self.dict_digitizer["digitizer_inputOffsetPercent1"][1].GetValue())
        numSamples = int(self.dict_digitizer["digitizer_numSamples"][1].GetValue())
        dt = 1.e3/float(self.dict_digitizer["digitizer_sampleRate"][1].GetValue())
        self.plotpanel.setLimitsX([0,numSamples*dt])
        self.plotpanel.setLimitsY(0, [(-1+inputOffset0)*inputMax0*0.001, (1+inputOffset0)*inputMax0*0.001])
        self.plotpanel.setLimitsY(1, [(-1+inputOffset1)*inputMax1*0.001, (1+inputOffset1)*inputMax1*0.001])

    def _readConfigFile(self, file):
        try:
            config = json.load(file)
        except:
            wx.LogError("Unable to parse json.")
            return

        for key, value in config.items():
            if key == "stepSizeAngle":
                self.angleRangePanel.setStepSize(value)
            elif key == "stepSizeY":
                self.yRangePanel.setStepSize(value)
            elif key == "rangesAngle":
                self.angleRangePanel.setRangesFromConfig(value)
            elif key == "rangesY":
                self.yRangePanel.setRangesFromConfig(value)
            else:
                for idict in [self.dict_output, self.dict_funcgen, self.dict_devices, self.dict_digitizer]:
                    if key in idict:
                        idict[key][1].SetValue(value)

        # Check if config file has old samplerate format in Hz and convert to MHz if that is the case
        if self.dict_digitizer["digitizer_sampleRate"][1].GetValue() != "":
            samplerate = float(self.dict_digitizer["digitizer_sampleRate"][1].GetValue())
            if samplerate > 5000:
                self.dict_digitizer["digitizer_sampleRate"][1].SetValue(str(samplerate/10**6))

        self._toggleChannels()

    def openConfigFile(self, event):
        with wx.FileDialog(self, "Open config file", wildcard="config files (*.conf)|*.conf",
                       style=wx.FD_OPEN | wx.FD_FILE_MUST_EXIST) as fileDialog:

            if fileDialog.ShowModal() == wx.ID_CANCEL: # User changed their mind
                return 

            # Proceed loading the file chosen by the user
            pathname = fileDialog.GetPath()
            try:
                with open(pathname, 'r') as file:
                    self._readConfigFile(file)
            except IOError:
                wx.LogError("Cannot open file '%s'." % file)

    def saveConfigFile(self, event):
        config_dict = self._constructConfigDict()

        with wx.FileDialog(self, "Save XYZ file", wildcard="config files (*.conf)|*.conf",
                       style=wx.FD_SAVE | wx.FD_OVERWRITE_PROMPT) as fileDialog:

            if fileDialog.ShowModal() == wx.ID_CANCEL: # User changed their mind
                return 

            # save the current contents in the file
            pathname = fileDialog.GetPath()
            try:
                with open(pathname, 'w') as file:
                    json.dump(config_dict, file, indent=4)
            except IOError:
                wx.LogError("Cannot save current data in file '%s'." % pathname)

    def onQuit(self, event):
        self.Close()    


if __name__ == "__main__":

    app = wx.App()

    theGUI = GUI(None, title='Diffuser Scan', size=wx.Size(1000, 1000))
    theGUI.Show()

    app.MainLoop()
