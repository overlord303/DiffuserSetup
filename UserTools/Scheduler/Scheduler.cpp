#include "Scheduler.h"

Scheduler::Scheduler():Tool()
{

}

bool Scheduler::Initialise(std::string configfile, DataModel &data)
{
  m_data = &data;
  m_log = m_data->Log;

  stateName[state::idle]               = "idle";
  stateName[state::init]               = "init";
  stateName[state::move]               = "move";  
  stateName[state::move_lin]           = "move_lin";
  stateName[state::move_rot]           = "move_rot";
  stateName[state::record]             = "record";
  stateName[state::record_single_init] = "record_single_init";
  stateName[state::record_single]      = "record_single";
  stateName[state::finalise]           = "finalise";
  stateName[state::home_lin]           = "home_lin";
  stateName[state::home_rot]           = "home_rot";
  stateName[state::zero_lin]           = "zero_lin";
  stateName[state::zero_rot]           = "zero_rot";
  stateName[state::end]                = "end";

  std::string socket_send;
  std::string socket_recv;
  m_data->vars.Get("zmqsocket_send", socket_send);
  m_data->vars.Get("zmqsocket_recv", socket_recv);

  context = new zmq::context_t(1);
  zmqsocket_send = new zmq::socket_t(*context, ZMQ_PUSH);
  zmqsocket_send->connect(socket_send);
  zmqsocket_recv = new zmq::socket_t(*context, ZMQ_SUB);
  zmqsocket_recv->connect(socket_recv);
  zmqsocket_recv->setsockopt(ZMQ_SUBSCRIBE, "", 0);

  std::string cfgfile;
  if(m_data->vars.Get("$1", cfgfile)) // Config file was specified on command line
  {
    useGUI = false;
    file = new std::ifstream(cfgfile.c_str());
    if(!file->good())
    {
      Log("Scheduler: ERROR, could not find specified config file! Exiting", 1, m_verbose);
      return false;
    }
    std::string config_str((std::istreambuf_iterator<char>(*file)),
                           std::istreambuf_iterator<char>());
    m_data->vars.JsonParser(config_str);
    Log("Scheduler: Config loaded from file " + configfile, 1, m_verbose);
    file->close();
    std::string rootfilename;
    if(m_data->vars.Get("$2", rootfilename))
      m_data->vars.Set("rootfilename", rootfilename);
  }
  else // Wait for config from GUI
  {
    useGUI = true;
    Log("Scheduler: Waiting to receive initialise config from GUI", 1, 1);
    zmq::message_t config_msg;
    std::string config_str;
    zmqsocket_recv->recv(&config_msg, 0);
    config_str = (char*)config_msg.data();
    m_data->vars.JsonParser(config_str);
    Log("Scheduler: Got initialise config from GUI!", 1, 1);
  }

  //-------------------------------------------------------------------------
  // Fix since Python Tools can't get std::string variables from ASCII stores
  //-------------------------------------------------------------------------
  std::string funcgen_IP;
  m_data->vars.Get("funcgen_IP", funcgen_IP);
  std::string delim = ".";
  int IPnum;
  for(int i=0; i<4; ++i)
  {
    IPnum = std::stoi(funcgen_IP.substr(0,funcgen_IP.find(delim)));
    funcgen_IP.erase(0, funcgen_IP.find(delim) + delim.length());
    m_data->vars.Set("funcgen_IP"+std::to_string(i), IPnum);
  }
  //-------------------------------------------------------------------------

  SetMode(state::idle);

  return true;
}

bool Scheduler::Execute()
{
  switch (m_data->mode)
  {
    case state::idle:
    {
      if(useGUI)
      {
        // Always update motor positions when returning to idle
        zmq::message_t msg = ZMQCreatePositionMessage(m_data->coord_angle, m_data->coord_y);
        zmqsocket_send->send(msg);
        
        zmq::message_t config_msg;
        std::string config_str;
        Log("Scheduler: Waiting to receive config from GUI", 1, 1);
        zmqsocket_recv->recv(&config_msg, 0);
        config_str = (char*)config_msg.data();
        m_data->tempstore.Delete();
        m_data->tempstore.JsonParser(config_str);
        double coord;
        if(m_data->tempstore.Get("move_linmotor", coord))
        {
          Log("Scheduler: Received linear motor move command from GUI", 1, m_verbose);
          SetMode(state::move_lin);

          m_data->coord_y = coord;
          break;
        }
        else if(m_data->tempstore.Get("move_rotmotor", coord))
        {
          Log("Scheduler: Received angular motor move command from GUI", 1, m_verbose);
          SetMode(state::move_rot);
          m_data->coord_angle = coord;
          break;
        }
        else if(m_data->tempstore.Has("home_linmotor"))
        {
          Log("Scheduler: Homing linear motor", 1, m_verbose);
          SetMode(state::home_lin);
          break;
        }
        else if(m_data->tempstore.Has("home_rotmotor"))
        {
          Log("Scheduler: Homing angular motor", 1, m_verbose);
          SetMode(state::home_rot);
          break;
        }
        else if(m_data->tempstore.Has("zero_linmotor"))
        {
          Log("Scheduler: Setting current linear motor position to zero", 1, m_verbose);
          SetMode(state::zero_lin);
          break;
        }
        else if(m_data->tempstore.Has("zero_rotmotor"))
        {
          Log("Scheduler: Setting current angular motor position to zero", 1, m_verbose);
          SetMode(state::zero_rot);
          break;
        }        
        else if(m_data->tempstore.Has("record_single"))
        {
          Log("Scheduler: Received record command from GUI", 1, m_verbose);
          m_data->vars.JsonParser(config_str);
          SetMode(state::record_single_init);
          break;
        }
        else
        {
          Log("Scheduler: Received config from GUI!", 1, m_verbose);
          m_data->vars.JsonParser(config_str);
        }
        m_data->tempstore.Delete();
      }

      SetMode(state::init);

      std::string angleRangesString;
      std::string yRangesString;
      double stepSizeAngle;
      double stepSizeY;
      if(!m_data->vars.Get("verbose", m_verbose)) m_verbose = 1;
      m_data->vars.Get("stepSizeAngle", stepSizeAngle);
      m_data->vars.Get("stepSizeY", stepSizeY);
      m_data->vars.Get("rangesAngle", angleRangesString);
      m_data->vars.Get("rangesY", yRangesString);

      std::vector<std::tuple<double,double>> angleRanges = ParseRanges(angleRangesString);
      std::vector<std::tuple<double,double>> yRanges = ParseRanges(yRangesString);

      if(angleRanges.size() == 0 || yRanges.size() == 0)
      {
        Log("Scheduler: No scan ranges found. Exiting.", 1, m_verbose);
        SetMode(state::end);
        m_data->vars.Set("StopLoop",1);
        break;
      }

      m_iterAngle.Initialise(stepSizeAngle, angleRanges);
      m_iterY.Initialise(stepSizeY, yRanges);
      break;
    }

    case state::init:
    {
      SetMode(state::move);
      m_data->coord_angle = m_iterAngle.GetPos();
      m_data->coord_y = m_iterY.GetPos();
      break;
    }

    case state::move_lin:
    case state::move_rot:
    {
      SetMode(state::idle);
      zmq::message_t msg = ZMQCreatePositionMessage(m_data->coord_angle, m_data->coord_y);
      zmqsocket_send->send(msg);
      break;
    }

    case state::move:
    {
      SetMode(state::record);
      break;
    }

    case state::record:
    {
      SetMode(state::move);
      zmq::message_t msg = ZMQCreateWaveformMessage("multi", m_data->coord_angle, m_data->coord_y, m_data->waveform_PMT, m_data->waveform_PD);
      zmqsocket_send->send(msg);

      if(!UpdateMotorCoords())
        SetMode(state::finalise);

      break;
    }

    case state::record_single_init:
    {
      SetMode(state::record_single);
      break;
    }

    case state::record_single:
    {
      SetMode(state::idle);
      zmq::message_t msg = ZMQCreateWaveformMessage("single", m_data->coord_angle, m_data->coord_y, m_data->waveform_PMT, m_data->waveform_PD);
      zmqsocket_send->send(msg);
      break;
    }

    case state::home_rot:
    case state::home_lin:
    case state::zero_rot:
    case state::zero_lin:
    {
      SetMode(state::idle);
      zmq::message_t msg = ZMQCreatePositionMessage(m_data->coord_angle, m_data->coord_y);
      zmqsocket_send->send(msg);
      break;
    }

    case state::finalise:
    {
      if(useGUI)
      {
        Log("Scheduler: Data collection finished. Returning to standby mode.", 1, m_verbose);
        SetMode(state::idle);
      }
      else
      {
        SetMode(state::end);
        m_data->vars.Set("StopLoop",1);
      }
      break;
    }
  }

  Log("Scheduler: Starting mode "+stateName[m_data->mode], 1, m_verbose);

  return true;
}

bool Scheduler::Finalise()
{
  Log("Scheduler: Finalising", 1, m_verbose);
  return true;
}

std::vector<std::tuple<double,double>> Scheduler::ParseRanges(std::string rangesString)
{
  std::vector<std::tuple<double,double>> ranges;
  size_t start;
  size_t end = 0;
  std::string tempstring;
  while((start = rangesString.find_first_not_of("|", end)) != std::string::npos)
  {
    end = rangesString.find("|", start);
    tempstring = rangesString.substr(start, end - start);
    int delim_pos = tempstring.find(",");
    double first = std::stod(tempstring.substr(0,delim_pos));
    double last = std::stod(tempstring.substr(delim_pos+1));
    ranges.push_back(std::tuple<double,double>(first,last));
  }

  return ranges;
}

bool Scheduler::UpdateMotorCoords()
{
  bool validPosition = true;
  if(!m_iterAngle.NextPos())
  {
    if(!m_iterY.NextPos())
    {
      validPosition = false;
    }
  }

  if(validPosition)
  {
    m_data->coord_angle = m_iterAngle.GetPos();
    m_data->coord_y = m_iterY.GetPos();
  }

  return validPosition;
}

zmq::message_t Scheduler::ZMQCreateWaveformMessage(std::string mode, double angle, double ypos, std::vector<double> waveform_PMT, std::vector<double> waveform_PD)
{
  std::tuple<std::string, double, double, std::vector<double>, std::vector<double>> msgtuple(mode, angle, ypos, waveform_PMT, waveform_PD);
  msgpack::sbuffer msgtuple_packed;
  msgpack::pack(&msgtuple_packed, msgtuple);
  zmq::message_t message(msgtuple_packed.size());
  std::memcpy(message.data(), msgtuple_packed.data(), msgtuple_packed.size());
  return message;
}

zmq::message_t Scheduler::ZMQCreatePositionMessage(double angle, double ypos)
{
  std::tuple<double, double> msgtuple(angle, ypos);
  msgpack::sbuffer msgtuple_packed;
  msgpack::pack(&msgtuple_packed, msgtuple);
  zmq::message_t message(msgtuple_packed.size());
  std::memcpy(message.data(), msgtuple_packed.data(), msgtuple_packed.size());
  return message;
}

void Scheduler::SetMode(state mode)
{
  m_data->mode = mode;
  m_data->vars.Set("state", mode);
}
