#include "Digitizer.h"

Digitizer::Digitizer():Tool(){}


bool Digitizer::Initialise(std::string configfile, DataModel &data)
{
  m_data= &data;
  m_log= m_data->Log;

  if(!m_data->vars.Get("verbose", m_verbose)) m_verbose = 1;

  // Open card
  cardHandle = spcm_hOpen("/dev/spcm0");
  if(!cardHandle)
  {
    Log("Digitizer: No card found!", 1, m_verbose);
    return false;
  }

  return true;
}

bool Digitizer::Execute()
{
  switch(m_data->mode)
  {
    case state::init:
    {
      StartDMA();
      break;
    }

    case state::record:
    {
      RecordWaveform();
      break;
    }

    case state::record_single_init:
    {
      StartDMA();
      break;
    }

    case state::record_single:
    {
      RecordWaveform();
      StopDMA();
      break;
    }

    case::finalise:
    {
      StopDMA();
      break;
    }
  }

  return true;
}

bool Digitizer::Finalise()
{
  spcm_vClose(cardHandle);

  return true;
}

bool Digitizer::StartDMA()
{
  m_data->vars.Get("digitizer_numChannels", m_numChannels);
  m_data->vars.Get("digitizer_numSamples", m_numSamples);
  m_data->vars.Get("funcgen_cycles", m_numAverages);
  m_data->vars.Get("digitizer_triggerLevel", m_triggerLevel);
  m_data->vars.Get("digitizer_inputRange0", m_inputRange0);
  m_data->vars.Get("digitizer_inputRange1", m_inputRange1);
  m_data->vars.Get("digitizer_inputOffsetPercent0", m_inputOffsetPercent0);
  m_data->vars.Get("digitizer_inputOffsetPercent1", m_inputOffsetPercent1);
  //double samplerate_MHz;
  //m_data->vars.Get("digitizer_sampleRate", samplerate_MHz);
  //m_sampleRate = samplerate_MHz;
  m_data->vars.Get("digitizer_sampleRate", m_sampleRate);

  if(m_numChannels < 2)
  {
    m_inputRange1 = 0;
    m_inputOffsetPercent1 = 0;
  }

  m_data->dt = 1./m_sampleRate;

  // FIFO mode buffer handling
  lNotifySize = 4096 * ceil(m_numSamples * m_numChannels / 4096.); // Can only collect samples in multiples of 4096
  llBufferSize = lNotifySize * (m_numAverages + 2); // software (DMA) buffer size

  // Define the data buffers
  dataBlock = (int8*)pvAllocMemPageAligned((uint64)llBufferSize);
  average_chan0 = new std::vector<double>(m_numSamples, 0);
  average_chan1 = new std::vector<double>(m_numSamples, 0);

  if(!dataBlock || !average_chan0 || !average_chan1)
  {
    Log("Digitizer: Memory allocation failed!", 1, m_verbose);
    spcm_vClose(cardHandle);
    return false;
  }

  if(m_numChannels > 1)
    spcm_dwSetParam_i32(cardHandle, SPC_CHENABLE,         CHANNEL0 | CHANNEL1);   // Channel 0 and 1 enabled
  else
    spcm_dwSetParam_i32(cardHandle, SPC_CHENABLE,         CHANNEL0);   // Channel 0 enabled

  // Channel 0
  spcm_dwSetParam_i32(cardHandle, SPC_AMP0,             m_inputRange0);                  // Max value in symmetric input range [mV]
  spcm_dwSetParam_i32(cardHandle, SPC_OFFS0,            -m_inputOffsetPercent0);         // Percent input range offset
  spcm_dwSetParam_i32(cardHandle, SPC_ACDC0,            0);                              // Set DC coupling

  // Channel 1
  if(m_numChannels > 1)
  {
    spcm_dwSetParam_i32(cardHandle, SPC_AMP1,           m_inputRange1);                  // Max value in symmetric input range [mV]
    spcm_dwSetParam_i32(cardHandle, SPC_OFFS1,          -m_inputOffsetPercent1);         // Percent input range offset
    spcm_dwSetParam_i32(cardHandle, SPC_ACDC1,          0);                              // Set DC coupling
  }

  spcm_dwSetParam_i32(cardHandle, SPC_CARDMODE,         SPC_REC_FIFO_MULTI);             // Multiple recording FIFO mode
  spcm_dwSetParam_i32(cardHandle, SPC_LOOPS,            0);                              // Endless
  spcm_dwSetParam_i32(cardHandle, SPC_SEGMENTSIZE,      lNotifySize/m_numChannels);      // Size of segment per trigger per channel
  spcm_dwSetParam_i32(cardHandle, SPC_POSTTRIGGER,      lNotifySize/m_numChannels - 32); // 32 samples pretrigger data for each segment
  spcm_dwSetParam_i32(cardHandle, SPC_TIMEOUT,          0);                              // No timeout
  spcm_dwSetParam_i32(cardHandle, SPC_TRIG_ORMASK,      SPC_TMASK_EXT0);                 // Trigger set to external input Ext0
  spcm_dwSetParam_i32(cardHandle, SPC_TRIG_ANDMASK,     0);                              // ...
  spcm_dwSetParam_i32(cardHandle, SPC_CLOCKMODE,        SPC_CM_INTPLL);                  // Clock mode internal PLL
  spcm_dwSetParam_i64(cardHandle, SPC_SAMPLERATE,       m_sampleRate);                   // Sampling rate in HZ
  spcm_dwSetParam_i32(cardHandle, SPC_CLOCKOUT,         0);                              // No clock output
  spcm_dwSetParam_i32(cardHandle, SPC_TRIG_EXT0_MODE,   SPC_TM_POS);                     // Rising edge trigger
  spcm_dwSetParam_i32(cardHandle, SPC_TRIG_EXT0_LEVEL0, m_triggerLevel);                 // External trigger level

  int64 samplerate_readout;
  spcm_dwGetParam_i64(cardHandle, SPC_SAMPLERATE,       &samplerate_readout);
  Log("Digitizer: The samplerate on the card is "+std::to_string(samplerate_readout)+" Hz");

  // Define transfer
  spcm_dwDefTransfer_i64(cardHandle, SPCM_BUF_DATA, SPCM_DIR_CARDTOPC, lNotifySize, dataBlock, 0, llBufferSize);

  // Start everything
  dwError = spcm_dwSetParam_i32(cardHandle, SPC_M2CMD, M2CMD_CARD_START | M2CMD_CARD_ENABLETRIGGER | M2CMD_DATA_STARTDMA);

  // Check for errors
  if (dwError != ERR_OK)
  {
    spcm_dwGetErrorInfo_i32(cardHandle, NULL, NULL, szErrorTextBuffer);
    Log("Digitizer: Error message: "+std::string(szErrorTextBuffer), 1, m_verbose);
    vFreeMemPageAligned(dataBlock, (uint64)llBufferSize);
    spcm_vClose(cardHandle);
    return false;
  }

  return true;
}

bool Digitizer::StopDMA()
{
  // Send card stop command
  dwError = spcm_dwSetParam_i32(cardHandle, SPC_M2CMD, M2CMD_CARD_STOP | M2CMD_DATA_STOPDMA);

  // Clean up memory
  Log("Digitizer: Finalising", 1, m_verbose);
  vFreeMemPageAligned(dataBlock, (uint64)llBufferSize);
  average_chan0->clear();
  average_chan1->clear();

  return true;
}

bool Digitizer::RecordWaveform()
{
  //auto start = high_resolution_clock::now();

  // Reset average vectors and count
  std::fill(average_chan0->begin(), average_chan0->end(), 0);
  std::fill(average_chan1->begin(), average_chan1->end(), 0);
  lAverageCount = 0;

  //for(int k=0; k<m_numAverages; ++k)
  //{
  //  spcm_dwSetParam_i32(cardHandle, SPC_M2CMD, M2CMD_CARD_FORCETRIGGER);
  //  usleep(1);
  //}

  // run the FIFO mode and loop through the data
  while(true)
  {
    // Wait for interrupt and check if we have new data
    if((dwError = spcm_dwSetParam_i32 (cardHandle, SPC_M2CMD, M2CMD_DATA_WAITDMA)) == ERR_OK)
    {
      spcm_dwGetParam_i32(cardHandle, SPC_M2STATUS,             &lStatus);
      spcm_dwGetParam_i64(cardHandle, SPC_DATA_AVAIL_USER_LEN,  &llAvailUser);
      spcm_dwGetParam_i64(cardHandle, SPC_DATA_AVAIL_USER_POS,  &llPCPos);

      if(lStatus & M2STAT_DATA_OVERRUN)
      {
        Log("Digitizer: Buffer overrun!", 1, m_verbose);
        break;
      }

      if(llAvailUser >= lNotifySize)
      {
        lAverageCount += 1;

        // Add latest waveforms to average data
        for(int i=0; i<m_numSamples; ++i)
        {
          if(m_numChannels > 1)
          {
            average_chan0->at(i) += (double)dataBlock[llPCPos + 2*i];
            average_chan1->at(i)  += (double)dataBlock[llPCPos + 2*i + 1];
          }
          else
            average_chan0->at(i) += (double)dataBlock[llPCPos + i];
        }

        // free the buffer
        spcm_dwSetParam_i32(cardHandle, SPC_DATA_AVAIL_CARD_LEN, lNotifySize);

        if(lAverageCount >= m_numAverages)
        {
          Log("Digitizer: Got all "+std::to_string(m_numAverages)+" triggers", 1, m_verbose);
          break;
        }
      }
    }

    // Something went wrong
    else
    {
      if(dwError == ERR_TIMEOUT)
        Log("Digitizer: Timeout!", 1, m_verbose);
      else
      {
        Log("Digitizer: Error: "+std::to_string(dwError), 1, m_verbose);
        break;
      }
    }
  }

  double scalefactor0 = m_inputRange0 * 0.001 / (127. * m_numAverages);
  double scalefactor1 = m_inputRange1 * 0.001 / (127. * m_numAverages);

  double offset0 = m_inputRange0 * m_inputOffsetPercent0 * 0.00001;
  double offset1 = m_inputRange1 * m_inputOffsetPercent1 * 0.00001;

  for(int i=0; i<m_numSamples; ++i)
  {
    average_chan0->at(i) = average_chan0->at(i) * scalefactor0 + offset0;
    average_chan1->at(i) = average_chan1->at(i) * scalefactor1 + offset1;
  }

  m_data->waveform_PMT = *average_chan0;
  m_data->waveform_PD  = *average_chan1;

  // auto stop = high_resolution_clock::now();
  // auto duration = duration_cast<microseconds>(stop - start);
  // std::cout << "Time taken: " << duration.count()/1000. << " milliseconds" << std::endl;

  return true;
}
